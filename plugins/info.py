#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import os, re
import configparser


@plugin
class Info(object):
    """Infobot. Use 'learn X as Y' and 'what is X'"""
    def __init__(self, server):
        self.require_auth = False
        self.server = server
        if 'infochar' in self.server.config:                     ##N# Sorry, this 
            self.plugin_trigger = self.server.config["infochar"] ##N# masterpiece is
        else:                                                    ##N# for backward
            self.plugin_trigger = "~"                            ##N# compatibility.
        self.server.pluginManager.loadPlugin("Auth")
        server.handle("message", self.handle_message)
        self.commands = ["what", "who", "learn", "forget", "fulldb"]
        self.server.handle("command", self.handle_command, self.commands)
        self.dbfile = self.server.config["confdir"] + "info.cfg"
        self.infodb = {}
        self.badchars = re.compile("[?]")

        self.configParser = configparser.RawConfigParser()
        if os.path.isfile(self.dbfile):
            self.configParser.read(self.dbfile)
            for k, v in self.configParser.items(self.server.config["network"]):
                self.infodb[k] = v

    def destroy(self):
        self.configParser = configparser.RawConfigParser()

        if os.path.isfile(self.dbfile):
            self.configParser.read(self.dbfile)

        fh = open(self.dbfile, "w")
        network = self.server.config["network"]
        if not self.configParser.has_section(network):
            self.configParser.add_section(network)

        for k, v in list(self.infodb.items()):
            self.configParser.set(network, k, v)

        self.configParser.write(fh)
        fh.close()

    def getInfo(self, key):
        for k, v in list(self.infodb.items()):
            if k.lower() == key.lower():
                return k, v

        return [None, None]

    def handle_message(self, channel, nick, message):
        """
        Handle messages not prefixed by comchar: react to things like
        <bob> !ask
        <bot> Ask your question, include all the details.
        <bot> !ask @ newbit
        <bot> newbie: Ask your question, include all the details.
        """
        message = message.strip()
        if message == "":              # empty message
            return
        if message[0] != self.plugin_trigger:  # message does not start with our trigger
            return
        message = message[1:].strip()  # work on the stuff after the trigger
        if message == "":              # ignore empty messages
            return
        if message.find("@") == -1:    # no nick handle
            key = message.strip()
            nick = ""
        else:                          # there is a nick handle
            list = message.split("@")
            key = list[0].strip()
            nick = list[1].strip()
        if key == "":                  # ignore empty keys
            return
        k, v = self.getInfo(key)       # actually get the factoid
        if k is not None and v is not None:
            if nick == "":
                self.server.doMessage(channel, "%s is %s" % (k, v))
            else:
                self.server.doMessage(channel, "%s: %s is %s" % (nick, k, v))
        else:  # tell the user in private that there is no such factoid (in-channel is annoying)
            self.server.doNotice(nick, "I don't know what " + key + " is.")

    def handle_command(self, channel, user, cmd, args, info_silent=False):

        def learn():
            if len(args) < 1:
                if not info_silent:
                    self.server.doMessage(channel, "...what should I learn?")
                return

            ####for i in range(0, len(args)):                   ##N# Useless because it strips 
            ####    args[i] = self.badchars.sub("", args[i])    #### the '?'' in the factoids.

            tmp = " ".join(args)
            ind = tmp.find(" as ")
            size = 4
            if ind == -1:
                ind = tmp.find(" as: ")
                size = 5
                if ind == -1:
                    if not info_silent:
                        self.server.doMessage(channel, "...what should I learn " + tmp + " as?")
                    return

            val = tmp[ind + size:]
            key = tmp[0:ind]
            key = key.replace("\t", "")
            k, v = self.getInfo(key)
            if k is not None and v is not None:
                if not info_silent:
                    self.server.doMessage(channel, "I already know what " + k + " is.")
                return

            self.infodb[key] = val
            if not info_silent:
                self.server.doMessage(channel, "Ok, I learned what " + key + " is.")

        def forget():
            if len(args) < 1:
                if not info_silent:
                    self.server.doMessage(channel, "...what should I forget?")
                return

            ####for i in range(0, len(args)):                   ##N# If not commented out it's not
            ####    args[i] = self.badchars.sub("", args[i])    ##N# possible to delete factoids with '?'

            key = " ".join(args).strip(" ")
            if key in self.infodb:
                del self.infodb[key]
                self.server.doMessage(channel, "Ok, I forgot what " + key + " is.")
            else:
                self.server.doMessage(channel, "Sorry, I don't know what " + key + " is.")
        if cmd == "what" or cmd == "who":
            for i in range(0, len(args)):
                args[i] = self.badchars.sub("", args[i])

            ind = 0
            for i in range(0, len(args)):
                if args[i] == "is" or args[i] == "are" or args[i] == "a" or args[i] == "an" or args[i] == "":
                    ind += 1
                else:
                    break

            nargs = []
            for i in range(ind, len(args)):
                if args[i] != "":
                    nargs.append(args[i])

            args = nargs

            if len(args) < 1:
                if not info_silent:
                    self.server.doMessage(channel, "...what should I tell you about?")
                return

            tmp = " ".join(args)
            k, v = self.getInfo(tmp)

            if k is not None and v is not None:
                self.server.doMessage(channel, user + ": " + k + ": " + v)
            else:
                if not info_silent:
                    self.server.doMessage(channel, user + ", I don't know what " + tmp + " is.")

        elif cmd == "learn":
            if not self.require_auth:
                learn()
            else:
                if self.server.getPlugin("auth").isMod(user):
                    learn()
                else:
                    self.server.doMessage(channel, user + ": You are not authorized to use this function. ")

        elif cmd == "forget":
            if not self.require_auth:
                forget()
            else:
                if self.server.getPlugin("auth").isMod(user):
                        forget()
                else:
                    self.server.doMessage(channel, user + ": You are not authorized to use this function. ")

        elif cmd == "fulldb":
            self.server.doMessage(channel, user + ": My database contains (separated by '|'): " + " | ".join(list(self.infodb.keys())))


