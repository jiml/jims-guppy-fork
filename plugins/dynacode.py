#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import sys


@plugin
class DynaCode(object):
    def __init__(self, server):
        self.server = server
        self.server.pluginManager.loadPlugin("auth")
        self.commands = ["py"]
        self.ownerOnly = 1
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if self.server.getPlugin("auth").isOwner(user) and cmd == "py":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return

            backup = sys.stdout
            myout = OutputBuffer()
            sys.stdout = myout
            try:
                exec(" ".join(args))
            except Exception as e:
                sys.stdout = backup
                self.server.doMessage(channel, user + ": " + e.__class__.__name__ + ": " + e.__str__())

            sys.stdout = backup
            for line in myout.getOutput():
                self.server.doMessage(channel, user + ": " + line)


class OutputBuffer(object):
    def __init__(self):
        self.__output = []

    def write(self, s):
        if s != "\n":
            self.__output.append(s)

    def getOutput(self):
        return self.__output
