#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# ddg module
# --gry

import urllib.request
#from urllib.error import HTTPError, URLError
#import socket
import json
import urllib.parse


@plugin
class ddg(object):
    """DuckDuckGo search tools. Currently just 0-click info."""
    def __init__(self, server):
        self.server = server
        self.commands = ["ddg"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if len(args) < 1:
            self.server.doMessage(channel, user + ": DuckDuckGo.com Zero-Click infoboxes search. Syntax: ddg <query>.")
            return
        try:
            request = "+".join(args)
            sock = urllib.request.urlopen("http://api.duckduckgo.com/?q=%s&o=json" % request)
            data = sock.read()
            sock.close()
            data = data.decode('utf-8')
            jl = json.loads(data)
#            print(json.dumps(jl,indent=2))
            if str(jl["AbstractText"]) != "":
                self.server.doMessage(channel, user + ": " + "%s %s)" % (str(jl["AbstractURL"]), str(jl["AbstractText"][0:200])))
            elif str(jl["Definition"]) != "":
                url = urllib.parse.unquote(str(jl["DefinitionURL"]))
                self.server.doMessage(channel, user + ": " + "%s %s" % (url, str(jl["Definition"])))
            else:
                self.server.doMessage(channel, "No results for " + request + '.')
        except Exception as e:
            self.server.doMessage(channel, user + ": " + str(e))
