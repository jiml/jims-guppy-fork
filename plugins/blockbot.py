#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import re
import time


@plugin
class Blockbot(object):
    """Automated channel flood/spam protection."""
    def __init__(self, server):
        self.server = server
        self.commands = []
        self.nick = self.server.config["nickname"]
        server.handle("message", self.handle_message)

        findlist = [
            'you will be  unable to connect to freenode  unless you are using sasl'
        ]
        self.mps_limit = 4
        self.storage_time = 25
        self.repeat_limit = 3
        self.repeat_1word = 4
        self.kick_myself = 1 # Set this to 0 if you don't want guppy to kick itself for flood

        # Compile Spam Strings
        self.findlist = []
        if findlist:
            for each in findlist:
                self.findlist.append(re.compile(each))

        # Load Default Data
        self.msglist = []
        #self.lastnot = ('BBot', time.time(), 'sdkljfls')

    def handle_message(self, channel, nick, message):
        """  Called when a message is sent. """
        # if the user is an owner, do nothing
        if self.server.getPlugin('auth').isOwner(nick):
            return

        self.msglist.insert(0, (nick, channel, message,
                                time.time()))

        # Check for spam strings
        ldata = message.lower()
        for each in self.findlist:
            if re.search(each, ldata):
                self.server.doKick(channel, nick, 'spam')
                return

        # Extract messages by this user
        user_msgs = []
        for msg in self.msglist:
            if msg[0] == nick:
                user_msgs.append((nick, msg[1], msg[2], msg[3]))

        # Check for flooding
        if self.get_mps(user_msgs) > self.mps_limit:
            if nick == self.nick and self.kick_myself == 0:
                1
            else:
                self.server.doKick(channel, nick, 'flood')
            self.msglist.pop(0)

        # Check for repeats
        strings = []
        repeats = 0
        for msg in user_msgs:
            if msg[2] not in strings:
                strings.append(msg[2])
            else:
                repeats += 1
        if repeats > self.repeat_limit - 1:
            if nick == self.nick and self.kick_myself == 0:
                1
            else:
                self.server.doKick(channel, nick, 'flood/repetition')
            self.msglist.pop(0)

        # Clear out old messages
        now = time.time()
        for msg in self.msglist:
            if now - msg[3] > self.storage_time:
                self.msglist.remove(msg)

    def get_mps(self, user_msgs):
        """Count the number of messages sent per second"""
        time_range = user_msgs[0][3] - user_msgs[-1][3]
        if time_range == 0:
            return 1
        return len(user_msgs) / time_range
