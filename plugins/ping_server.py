#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import irc
import time
import threading


@plugin
class Ping_server(object):
    def __init__(self, server):
        # Variables.
        self.server = server
        self.network = self.server.config["network"]
        self.commands = []
        self.loop = True
        self.ponged = False
        self.timeLoop = 60

        def loop(plugin, server):
            time.sleep(10)  # wait for bot to connect to network
            timeWaited = 0
            while self.loop:
                if timeWaited >= self.timeLoop:
                    self.pingServer()
                    timeWaited = 0
                time.sleep(10)
                timeWaited += 10
        self.t1 = threading.Thread(target=loop, args=(self, server,))
        self.t1.daemon = True
        self.t1.start()
        server.handle("data", self.handle_data)

    def pingServer(self):
        if self.ponged == False:
            self.server.doQuit()
            irc.IRC(self.server.config)

        self.server.sendLine('ping :0000')
        self.ponged = False

    def handle_data(self, network, data):
        self.ponged = True
